import { getDefaultOptions, Parser, ParserOptions } from '../src/parser'
import { normalizeCritter } from '../src/parser/common'
import parsedDragon from './data/parsed/ancient-gold-dragon.json'
import parsedGoblin from './data/parsed/goblin.json'
import parsedKraken from './data/parsed/kraken.json'
import dragon from './data/raw/ancient-gold-dragon.json'
import goblin from './data/raw/goblin.json'
import kraken from './data/raw/kraken.json'

const options: ParserOptions = {
  ...getDefaultOptions(),
  conformProficiency: 'conformProfToCr',
  deriveProficiencyFromModifiers: true,
  linkActions: true,
}

test('Parse Goblin', () => {
  expect(Parser.parseCritter(options)(normalizeCritter(goblin))).toEqual(parsedGoblin)
})

test('Parse Ancient Gold Dragon', () => {
  expect(Parser.parseCritter(options)(normalizeCritter(dragon))).toEqual(parsedDragon)
})

test('Parse Kraken', () => {
  expect(Parser.parseCritter(options)(normalizeCritter(kraken))).toEqual(parsedKraken)
})
