
# CritterDB Parser

A utility module to parse CritterDB statblocks into a more consistent data structure. Utilizes [compromise-js](https://github.com/spencermountain/compromise) to parse relevant data from action and feature descriptions.

## Installation

    npm install --save @nystik/critterdb-parser

## Basic usage

The parser takes an object as exported from CritterDB in json format and returns a parsed statblock.
```typescript
import { Parser, getDefaultOptions } from  '@nystik/critterdb-parser'

// The critters fetched via CritterDB's API lack some derived properties, 
// use normalizeCritter() to ensure the types are compatible
const critterData = Parser.normalizeCritter(data) 
const statblock = Parser.parseCritter(getDefaultOptions())(critterData)
```
## Options

- `conformProficiency`: (default: `'ignore'`)
	- `'conformProfToCr'` in case of a discrepancy adjust proficiency bonus to match challenge rating.
	- `'conformCrToProf'` in case of a discrepancy adjust challenge rating to match proficiency bonus.
	- `'ignore'` ignore discrepancy and leave both proficiency bonus and challenge rating untouched.
- `deriveProficiencyFromModifiers`: (default: `false`) will attempt to derive skill/save proficiency and expertise from flat modifiers.
- `enforceMinimumProficiency`: (default: `true`) if proficiency bonus is lower than 2 set proficiency bonus to 2.
- `linkActions`: (default: `false`) attempt to parse actions used in multiattack and legendary actions.

## Known Issues/Limitations

- The matcher used when linking actions to **multiattack** and **legendary actions** is unreliable and will frequently give the wrong number of actions.
- The parser doesn't distinguish between and/or when parsing damage, other than in the case for versatile damage. It will either ignore the conditional damage or add it as additional damage instead.
- When parsing "multi actions" (i.e. breath weapon, eye rays, etc.) the parser assumes each item in the list starts with a name followed by a period. If the items do not start this way it will parse the entire first sentance as the name of the action.
   
## License

This project is licensed under the MIT license, see [LICENSE](https://gitlab.com/dnd-5e/critterdb-parser/-/blob/develop/LICENSE) for details.