# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.7.6] - 2021-11-10

### Added

- changelog

### Fixed

- bug in dice pattern
- bug in legendary action parser

## [0.7.5] - 2021-11-10

### Added

- everything, didn't keep track before this.
