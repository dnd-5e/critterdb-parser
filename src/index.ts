import 'ts-polyfill/lib/es2016-array-include'
import 'ts-polyfill/lib/es2017-object'
import 'ts-polyfill/lib/es2019-array'
import 'ts-polyfill/lib/es2019-object'
import 'ts-polyfill/lib/es2019-string'

export * from '@parser'
export * from '@typing'
export * from '@utils'
