type proficiencyConformity = 'conformProfToCr' | 'conformCrToProf' | 'ignore'

interface ParserOptions {
  /** Alter proficiency to match CR or vice versa or leave them discrepant */
  conformProficiency: proficiencyConformity
  /** Derive if creature has proficiency in skills or saving throws based on modifier */
  deriveProficiencyFromModifiers: boolean
  /** if proficiency bonus is lower than 2 change it to 2 */
  enforceMinimumProficiency: boolean
  /** try to parse actions used in multiattack and legendary actions */
  linkActions: boolean
}

const defaultOptions: ParserOptions = {
  conformProficiency: 'ignore',
  deriveProficiencyFromModifiers: false,
  enforceMinimumProficiency: true,
  linkActions: false,
}

const getDefaultOptions = (): ParserOptions => ({ ...defaultOptions })

export { ParserOptions, getDefaultOptions }
