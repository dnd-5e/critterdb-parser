import { ParserOptions } from '@parser/options'
import { parseAction } from '@parser/abilities/action'
import { isActionList, parseActionList } from '@parser/abilities/action-list'
import { isBonusAction } from '@parser/abilities/is-bonus-action'
import { parseLegendaryActions } from '@parser/abilities/legendary-actions'
import { parseSpellcastingFeatures } from '@parser/abilities/spellcasting'
import { linkActions } from '@parser/abilities/link-actions'
import { Abilities, Action, ActivationType, critterDB, LegendaryActions, Spellcasting } from '@typing'

const parser = (actions: critterDB.Action[], activation: ActivationType): Action[] =>
  actions.flatMap((a) => (isActionList(a) ? parseActionList(activation)(a) : parseAction(activation)(a)))

const featureParser = (actions: critterDB.Action[]): Action[] =>
  actions.flatMap((a) => (isBonusAction(a) ? parser([a], 'bonus') : parser([a], null)))

const parseAbillties =
  (options: ParserOptions) =>
  (critter: critterDB.Critter): Abilities => {
    const { stats } = critter
    const spellcastingFeatures: Spellcasting[] = parseSpellcastingFeatures(critter)

    const features = featureParser(
      stats.additionalAbilities.filter((a) => !a.name.toLowerCase().includes('spellcasting'))
    )
    const actions = [...parser(stats.actions, 'action'), ...parser(stats.reactions, 'reaction')]
    const legendaryActions: LegendaryActions = parseLegendaryActions(critter.stats)

    if (options.linkActions) {
      const linker = linkActions(actions.filter((a) => a.activation === 'action').map((a) => a.name))
      legendaryActions.actions = legendaryActions.actions.map((a) => {
        a.linked = linker(a.description)
        return a
      })

      const multiattack = actions.find((a) => a.name.toLowerCase().includes('multiattack'))
      if (multiattack) {
        multiattack.linked = linker(multiattack.description)
      }
    }

    return {
      actions,
      features,
      legendaryActions,
      spellcastingFeatures,
    }
  }

export { parseAbillties }
