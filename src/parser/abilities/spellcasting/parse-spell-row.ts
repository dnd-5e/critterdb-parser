import { ExtendedDocument, nlp } from '@parser/nlp'
import { Spell, SpellSlots } from '@typing'

const matchSlots = `[<level>#Value] level? [<slots>#Value] (slot|slots)`
const matchUses = `[<uses>#Value] day`

const defaultData = { level: 0, slots: 0, uses: 0 }

const isCantrip = (text: string): boolean => text.includes('cantrip')
const isAtWill = (text: string): boolean => text.includes('at will')
const isAtWillOrCantrip = (text: string): boolean => isAtWill(text) || isCantrip(text)

const extractSlotsAndUses = (doc: ExtendedDocument): { level: number; slots: number; uses: number } => {
  const result = { ...defaultData }
  const slots = doc.match(matchSlots)
  if (slots.found) {
    result.level = Number(slots.groups('level').toNumericCardinal!().text() || 0)
    result.slots = Number(slots.groups('slots').toNumericCardinal!().text() || 0)
  }

  const uses = doc.match(matchUses)
  if (uses.found) {
    result.uses = Number(uses.groups('uses').toNumericCardinal!().text() || 0)
  }
  return result
}

const extractSpells =
  (level: number, atWill: boolean, uses: number) =>
  (doc: ExtendedDocument): Spell[] => {
    return (
      doc
        .text()
        ?.split(':')[1]
        ?.split(',')
        ?.map((s) => ({ name: s.trim(), level, atWill, uses })) ?? []
    )
  }

const parseSpellRow = (text: string, innate: boolean): { slots: SpellSlots; spells: Spell[] } => {
  const doc = nlp(text)
  const { level, slots: num, uses } = !isAtWillOrCantrip(text) ? extractSlotsAndUses(doc) : { ...defaultData }
  const spells = extractSpells(level, innate, uses)(doc)
  const slots = level ? { [level]: num } : {}
  return { slots, spells }
}

export { parseSpellRow }
