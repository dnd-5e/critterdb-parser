import { critterDB, Spell, Spellcasting, SpellSlots } from '@typing'
import { stripHtml } from '@utils'
import { parseAtWill } from './parse-at-will'
import { parseSpellRow } from './parse-spell-row'
import { parseSpellStats } from './parse-spell-stats'

const prepareText = (text: string) => stripHtml(text.replace(/-|\.|\//g, ' ')).toLowerCase()

const parseSpellcastingFeatures = (critter: critterDB.Critter): Spellcasting[] => {
  const { additionalAbilities } = critter.stats
  const spellcasting = additionalAbilities.filter((f) => f.name.toLowerCase().includes('spellcasting'))

  return spellcasting.map((s) => parseSpellcastingFeature(s))
}

const parseSpellcastingFeature = (spellcasting: critterDB.Ability): Spellcasting => {
  const { name, description } = spellcasting
  const prepped = prepareText(description)
  const [meta, ...rows] = prepped.split('\n')

  const { innate, pact, ability, level, save, bonus } = parseSpellStats(prepped)

  let allSlots: SpellSlots = {}
  let allSpells: Spell[] = []
  for (const r of rows) {
    if (r) {
      const { slots, spells } = parseSpellRow(r, innate)
      allSpells = allSpells.concat(spells)
      allSlots = { ...allSlots, ...slots }
    }
  }
  allSpells = allSpells.concat(parseAtWill(meta))

  return {
    name,
    description,
    innate,
    pact,
    ability,
    level,
    save,
    bonus,
    slots: allSlots,
    spells: allSpells,
  }
}

export { parseSpellcastingFeatures, parseSpellcastingFeature }
