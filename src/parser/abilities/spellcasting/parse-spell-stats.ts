import { ExtendedDocument, nlp } from '@parser/nlp'
import { Spellcasting } from '@typing'

const matchLevel = '[<level>#Value] level? spellcaster'
const matchAbility = '[<ability>#Ability]'
const matchSave = 'DC is? [<save>#Cardinal]'
const matchBonus = ['[<bonus>#Cardinal] to hit', 'bonus is [<bonus>#Cardinal]']
//const matchClassStats = `[<level>#Value] level? spellcaster * ${matchAbilityAndSave}`
//const matchInnateStats = `innate * ${matchAbilityAndSave}`

type SpellcastingMeta = Omit<Spellcasting, 'name' | 'description' | 'slots' | 'spells'>

const defaultData: SpellcastingMeta = {
  innate: false,
  pact: false,
  level: 0,
  save: 0,
  bonus: 0,
  ability: '',
}

const isInnate = (text: string): boolean => text.includes('innate')
const isPact = (text: string): boolean => text.includes('warlock')

const extractStats = (doc: ExtendedDocument): Partial<SpellcastingMeta> => {
  const ability = doc.match(matchAbility).groups('ability').out('array')?.[0] ?? ''
  const save = Number(doc.match(matchSave).groups('save').toNumericCardinal!().out('array')?.[0] || 0)
  const level = Number(doc.match(matchLevel).groups('level').toNumericCardinal!().out('array')?.[0] || 0)
  const bonus = Number(
    matchBonus
      .flatMap((p) => doc.match(p).groups('bonus').toNumericCardinal!().out('array'))
      .filter((n) => Number.isInteger(Number(n)))?.[0] || 0
  )

  return {
    level,
    save,
    bonus,
    ability,
  }
}

const parseSpellStats = (text: string): SpellcastingMeta => {
  const innate = isInnate(text)
  return {
    ...defaultData,
    ...extractStats(nlp(text).normalize()),
    innate,
    pact: !innate ? isPact(text) : false,
  }
}

export { parseSpellStats, SpellcastingMeta }
