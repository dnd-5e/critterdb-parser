import { ExtendedDocument, nlp } from '@parser/nlp'
import { Spell } from '@typing'
import { splitSpellList } from '@utils'

const matchAtWill = `cast [<spells>*] at will`

const extractSpells = (doc: ExtendedDocument): string[] => {
  const match = doc.match(matchAtWill)

  if (match.found) {
    return splitSpellList(match.groups('spells').text())
  }
  return []
}

const parseAtWill = (text: string): Spell[] => {
  const doc = nlp(text)
  return extractSpells(doc).map((s) => ({ name: s, level: 0, atWill: true, uses: 0 }))
}

export { parseAtWill }
