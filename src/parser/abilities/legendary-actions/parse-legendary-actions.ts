import { parseAction } from '@parser/abilities/action'
import { isActionList, parseActionList } from '@parser/abilities/action-list'
import { ExtendedDocument, nlp } from '@parser/nlp'
import { Action, critterDB, LegendaryActions } from '@typing'

const matchCharges = `[<charges>#Value] legendary? actions`

const extractCharges = (doc: ExtendedDocument): number => {
  const match = doc.match(matchCharges)
  if (match.found) {
    return Number(match.groups('charges').toNumericCardinal!().text() || 0)
  }
  return 0
}

const parseLegendaryActions = (stats: critterDB.Stats): LegendaryActions => {
  const { legendaryActionsDescription: description, legendaryActions } = stats
  const doc = nlp(description)

  const actions: Action[] = legendaryActions.flatMap((a) =>
    isActionList(a) ? parseActionList('legendary')(a) : parseAction('legendary')(a)
  )

  const charges = extractCharges(doc) || actions.length > 0 ? stats.legendaryActionsPerRound : 0
  return {
    charges,
    description,
    actions,
  }
}

export { parseLegendaryActions }
