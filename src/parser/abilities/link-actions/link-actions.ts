import { ExtendedDocument, ExtendedNlp, generateActionLexicon, getNlp } from '@parser/nlp'
import { LinkedAction } from '@typing'

const matchAction = `[<count>#Value?] [<action>#Action?] [<action>#Action]`

const matchGeneric = `[<count>(#Value|a)] [<action>(melee|ranged|weapon|attack|attacks)] `

const extractActions =
  (pattern: string) =>
  (doc: ExtendedDocument): LinkedAction[] => {
    const match = doc.match(pattern)

    const actions: LinkedAction[] = []
    match.forEach((a) => {
      const num = Number(a.groups('count').toNumericCardinal!().text())
      const count = num > 0 ? num : 1
      const action = a.groups('action').text()

      if (count && action) {
        actions.push({ count, action })
      }
    })
    return actions
  }

const actionLinker =
  (nlp: ExtendedNlp) =>
  (description: string): LinkedAction[] => {
    const doc = nlp(description.toLowerCase())

    return [...extractActions(matchAction)(doc), ...extractActions(matchGeneric)(doc)]
  }

const linkActions = (names: string[]) => {
  const lexicon = generateActionLexicon(names.flatMap((s) => [s.toLowerCase(), `${s.toLowerCase()}s`]))
  const nlp = getNlp(lexicon)

  return actionLinker(nlp)
}

export { linkActions }
