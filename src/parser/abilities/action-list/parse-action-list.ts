import { parseAction } from '@parser/abilities/action'
import { nlp } from '@parser/nlp'
import { Action, ActivationType, critterDB } from '@typing'
import { stripHtml } from '@utils'

const isActionList = (action: critterDB.Action): boolean => {
  return nlp(action.description.split('\n')[0] ?? action.description).match('of the following').found
}

const parseActionList =
  (activation: ActivationType) =>
  (action: critterDB.Action): Action[] => {
    const { name, description, descriptionHtml, $$hashKey } = action
    const [main, ...parts] = description.split('\n')

    const mainAction = parseAction(activation)({ name, description: main, descriptionHtml, $$hashKey })

    const actions: Action[] = []
    for (const p of parts) {
      const components = stripHtml(p).split('.')
      const isNumberedList = Number.isInteger(Number(components[0]))
      const name = isNumberedList ? components[1]?.trimStart() ?? '' : components[0]?.trimStart() ?? ''

      const description = components
        .slice(isNumberedList ? 2 : 1)
        .join('.')
        .trimStart()
      const action = parseAction(activation)({ name, description, descriptionHtml, $$hashKey })
      actions.push(action)
    }

    return [mainAction, ...actions]
  }

export { parseActionList, isActionList }
