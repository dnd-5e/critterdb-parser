import { critterDB } from '@typing'

const isBonusAction = (action: critterDB.Action) => {
  return action.description.toLowerCase().includes('bonus action')
}

export { isBonusAction }
