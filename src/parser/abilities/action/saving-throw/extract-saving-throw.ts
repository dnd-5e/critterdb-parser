import { extractOngoingDamage } from '@parser/abilities/action/attack'
import { extractDamageRolls } from '@parser/abilities/action/damage-roll'
import { damageRollComparator } from '@parser/common'
import { ExtendedDocument, nlp } from '@parser/nlp'
import { AbilitySave } from '@typing'
import { arrayDiff } from '@utils'

const prepareText = (text: string) => text.replace(/escape dc/g, '')

const extractSavingThrows = (doc: ExtendedDocument): AbilitySave[] => {
  let sections = prepareText(doc.text().toLowerCase())
    .split('dc')
    .slice(1)
    .map((s) => `dc${s}`)

  const rolls: AbilitySave[] = []
  for (const section of sections) {
    const sDoc = nlp(section)
    let saveMatch = sDoc.match('dc [<dc>#Cardinal] [<ability>#Ability] (save|saving)')

    const ability = saveMatch.groups('ability').text()
    const dc = Number(saveMatch.groups('dc').text() || 0)

    let ongoing = extractOngoingDamage(sDoc)
    const damage = [...arrayDiff(extractDamageRolls('', '')()(sDoc), [...ongoing], damageRollComparator), ...ongoing]

    const save: AbilitySave = {
      ability,
      dc,
      damage,
    }
    if (ability || dc || damage) {
      rolls.push(save)
    }
  }
  return rolls
}

export { extractSavingThrows }
