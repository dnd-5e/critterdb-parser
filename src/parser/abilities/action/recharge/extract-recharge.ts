import { nlp } from '@parser/nlp'
import { stripHtml } from '@utils'

const prepareText = (text: string) => stripHtml(text.replace(/-|\.|\/|\(|\)/g, ' ')).toLowerCase()

const matchRecharge = `recharge [<recharge>#Cardinal? #Cardinal]`

const extractRecharge = (text: string): number[] => {
  const match = nlp(prepareText(text)).match(matchRecharge)
  return match.toNumericCardinal!()
    .out('array')
    .map((n) => Number(n))
}

export { extractRecharge }
