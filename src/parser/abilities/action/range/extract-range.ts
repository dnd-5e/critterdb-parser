import { ExtendedDocument } from '@parser/nlp'
import { Range } from '@typing'

const extractRange = (doc: ExtendedDocument): Range => {
  let match = doc.match('range? [<value>#Fraction] [<unit>#Unit]')
  if (match.found) {
    const fraction = match.groups('value').text()
    const [normal, max] = fraction.split('/').map((s) => Number(s))
    return {
      normal,
      max,
      unit: match.groups('unit').text(),
    }
  }
  return { normal: 0, max: 0, unit: '' }
}

export { extractRange }
