import { ExtendedDocument } from '@parser/nlp'
import { UnitValue } from '@typing'

const extractReach = (doc: ExtendedDocument): UnitValue[] => {
  let match = doc.match('reach? [<value>#Cardinal] [<unit>#Unit] !#AreaShape')

  const reaches: UnitValue[] = []
  match.forEach((m) => {
    const value = Number(m.groups('value').toNumericCardinal!().text() ?? 0)
    const unit = m.groups('unit').text()

    reaches.push({
      value,
      unit,
    })
  })
  return reaches
}

export { extractReach }
