import { ExtendedDocument } from '@parser/nlp'
import { UnitValue } from '@typing'

const matchDuration = `[<value>#Value] [<duration>#Duration]`

const extractDuration = (doc: ExtendedDocument): UnitValue[] => {
  const match = doc.stripHyphens!().match(matchDuration)

  const durations: UnitValue[] = []
  match.forEach((m) => {
    const value = Number(m.groups('value').toNumericCardinal!().text() || 0)
    const unit = m.groups('duration').text()

    durations.push({
      value,
      unit,
    })
  })
  return durations
}

export { extractDuration }
