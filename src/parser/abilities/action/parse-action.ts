import { extractOngoingDamage, isAttack, parseAttack } from '@parser/abilities/action/attack'
import { damageRollComparator, extractCharges, rollComparator } from '@parser/common'
import { nlp } from '@parser/nlp'
import { Action, ActivationType, critterDB, LinkedAction } from '@typing'
import { arrayDiff } from '@utils'
import { extractCost } from './cost'
import { extractDamageRolls } from './damage-roll'
import { extractDuration } from './duration'
import { extractRange } from './range'
import { extractReach } from './reach'
import { extractRecharge } from './recharge'
import { extractRolls } from './roll'
import { extractSavingThrows } from './saving-throw'
import { defaultTarget, extractTarget } from './target'

const prepareText = (text: string) => text.replace(/[\.\(\)]/g, ' ')

const parseAction =
  (activation: ActivationType) =>
  (action: critterDB.Action): Action => {
    const { name, description } = action

    const doc = nlp(prepareText(description)).stripHtml!().normalizeMath!()
    const reach = extractReach(doc.fixUnits!())[0] || { value: 0, unit: '' }
    const range = extractRange(doc.fixUnits!())

    const attack = isAttack(action) ? parseAttack(action) : null
    const savingThrows = extractSavingThrows(doc)

    const attackDamage = attack?.damage ?? []
    const attackVersatile = attack?.versatile ?? []
    const saveDamage = savingThrows.flatMap((s) => s.damage)

    const ongoingDamage = arrayDiff(
      extractOngoingDamage(doc),
      [...attackDamage, ...attackVersatile, ...saveDamage],
      damageRollComparator
    )

    const damageRolls = [
      ...arrayDiff(
        extractDamageRolls('', '')()(doc).filter((a) => a.damageType),
        [...attackDamage, ...attackVersatile, ...saveDamage, ...ongoingDamage],
        damageRollComparator
      ),
      ...ongoingDamage,
    ]

    const otherRolls = arrayDiff(
      extractRolls(doc),
      [...attackDamage, ...attackVersatile, ...saveDamage, ...ongoingDamage, ...damageRolls],
      rollComparator
    )

    const limitedUses = extractCharges(name)
    const target = extractTarget(doc.fixUnits!())?.[0] || { ...defaultTarget }
    const duration = extractDuration(doc.fixUnits!())[0] || { unit: '', value: 0 }

    const recharge = extractRecharge(name)
    const cost = activation === 'legendary' ? extractCost(name) : 0

    const linked: LinkedAction[] = []

    return {
      name,
      description,
      reach,
      range,
      activation,
      attack,
      savingThrows,
      damageRolls,
      otherRolls,
      duration,
      limitedUses,
      target,
      recharge,
      cost,
      linked,
    }
  }

export { parseAction }
