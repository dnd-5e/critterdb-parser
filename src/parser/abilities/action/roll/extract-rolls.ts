import { dicePattern } from '@parser/constants'
import { ExtendedDocument } from '@parser/nlp'
import { Roll } from '@typing'

const rollMatch = `[<dice>${dicePattern}]`

const prepareText = (text: string) => text.replace(/[\(\)]/g, '')

const extractRolls = (doc: ExtendedDocument): Roll[] => {
  let match = doc.match(rollMatch)

  const rolls: Roll[] = match
    .groups('dice')
    .out('array')
    .map((r) => ({
      formula: r,
    }))

  return rolls
}

export { extractRolls }
