import { nlp } from '@parser/nlp'

const matchCost = `(cost|costs) [<cost>#Value]`

const extractCost = (text: string): number => {
  const match = nlp(text.toLowerCase()).match(matchCost)
  const num = Number(match.groups('cost').toNumericCardinal!().text())
  return num > 0 ? num : 1
}

export { extractCost }
