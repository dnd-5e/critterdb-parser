import { dicePattern } from '@parser/constants'
import { ExtendedDocument } from '@parser/nlp'
import { DamageRoll } from '@typing'

const damageRollMatch = `[<average>#Cardinal?] [<dice>${dicePattern}] [<damageType>#DamageType?] damage?`

const extractDamageRolls =
  (patternPrefix: string, patternSuffix: string) =>
  (ongoing: boolean = false) =>
  (doc: ExtendedDocument): DamageRoll[] => {
    const pattern = `${patternPrefix}${damageRollMatch}${patternSuffix}`
    let match = doc.match(pattern)

    const rolls: DamageRoll[] = []
    match.forEach((m) => {
      const damageType = m.groups('damageType').toLowerCase().text()
      const formula = m.groups('dice').text()

      rolls.push({
        damageType,
        formula,
        ongoing,
      })
    })
    return rolls
  }

export { extractDamageRolls }
