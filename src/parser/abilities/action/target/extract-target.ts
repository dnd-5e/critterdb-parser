import { ExtendedDocument } from '@parser/nlp'
import { Target } from '@typing'

const matchTargetShape = `[<value>#Value] [<unit>#Unit] [<radius>radius?] [<shape>#AreaShape]`
const matchTargetCreature = `[<value>#Value] [<type>(target|targets|creature|creatures|humanoid|humanoids|enemy|enemies|ally|allies)]`

const defaultTarget: Target = {
  area: { shape: '', unit: '', value: 0 },
  count: 0,
  type: '',
}

const extractTarget = (doc: ExtendedDocument): Target[] => {
  const shapeMatch = doc.stripHyphens!().match(matchTargetShape)
  const creatureMatch = doc.stripHyphens!().match(matchTargetCreature)

  const shapes: Partial<Target>[] = []
  shapeMatch.forEach((m) => {
    const shape = m.groups('shape').text()

    const area = {
      shape: shape ? shape : m.groups('radius').text(),
      unit: m.groups('unit').text(),
      value: Number(m.groups('value').toNumericCardinal!().text() ?? 0),
    }

    shapes.push({
      area,
    })
  })

  const creatures: Partial<Target>[] = []
  creatureMatch.forEach((m) => {
    const count = Number(m.groups('value').toNumericCardinal!().text() || 0)
    const type = m.groups('type').text()

    creatures.push({
      count,
      type,
    })
  })

  let targets: Target[] = []
  if (shapes.length <= creatures.length) {
    for (const [i, s] of shapes.entries()) {
      creatures[i] = { ...defaultTarget, ...creatures[i], ...s }
    }
    targets = creatures as Target[]
  } else {
    for (const [i, c] of creatures.entries()) {
      shapes[i] = { ...defaultTarget, ...shapes[i], ...c }
    }
    targets = shapes as Target[]
  }

  return targets
}

export { extractTarget, defaultTarget }
