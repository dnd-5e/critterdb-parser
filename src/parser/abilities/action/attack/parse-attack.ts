import { nlp } from '@parser/nlp'
import { Attack, critterDB } from '@typing'
import { extractAttackBonus } from './attack-bonus'
import { extractAttackType } from './attack-type'
import { extractExtraDamage, extractMainDamage, extractOngoingDamage, extractVersatileDamage } from './damage'

const prepareText = (text: string) => text.replace(/\./g, ' ')

const parseAttack = (action: critterDB.Action): Attack => {
  const str = action.description.toLowerCase().split('dc')[0] || action.description.toLowerCase()
  const doc = nlp(prepareText(str)).stripHtml!().normalizeMath!()
  const { melee, ranged, spell, weapon } = extractAttackType(doc)
  const bonus = extractAttackBonus(doc)
  const damage = [...extractMainDamage(doc), ...extractExtraDamage(doc), ...extractOngoingDamage(doc)]
  const versatile = extractVersatileDamage(doc)

  return {
    melee,
    ranged,
    spell,
    weapon,
    bonus,
    damage,
    versatile,
  }
}

export { parseAttack }
