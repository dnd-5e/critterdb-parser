import { ExtendedDocument } from '@parser/nlp'

const extractAttackType = (
  doc: ExtendedDocument
): { weapon: boolean; spell: boolean; melee: boolean; ranged: boolean } => {
  let match = doc.toLowerCase().match('[<type>#AttackType] or? [<type>#AttackType?] [<weapon>(weapon|spell)] attack')

  const types = match.groups('type').out('array')
  const weaponType = match.groups('weapon').text()

  const melee = types.includes('melee')
  const ranged = types.includes('ranged')
  const weapon = weaponType === 'weapon'
  const spell = weaponType === 'spell'

  return {
    melee,
    ranged,
    weapon,
    spell,
  }
}

export { extractAttackType }
