import { ExtendedDocument } from '@parser/nlp'

const extractAttackBonus = (doc: ExtendedDocument): number => {
  let match = doc.match('[<bonus>#Cardinal] to hit')
  return Number(match.groups('bonus').toNumericCardinal!().text() ?? 0)
}

export { extractAttackBonus }
