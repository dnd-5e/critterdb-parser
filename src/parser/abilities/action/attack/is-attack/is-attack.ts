import { nlp } from '@parser/nlp'
import { critterDB } from '@typing'
import { extractAttackType } from '@parser/abilities/action/attack/attack-type'

const isAttack = (action: critterDB.Action) => {
  const doc = nlp(action.description)
  const { melee, ranged, spell, weapon } = extractAttackType(doc.stripHtml!())
  return melee || ranged || spell || weapon
}

export { isAttack }
