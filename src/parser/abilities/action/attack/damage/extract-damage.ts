import { extractDamageRolls } from '@parser/abilities/action/damage-roll'
import { ExtendedDocument } from '@parser/nlp'
import { DamageRoll } from '@typing'

const extractMainDamage = (doc: ExtendedDocument): DamageRoll[] => {
  return extractDamageRolls('hit ', '')()(doc)
}

const extractVersatileDamage = (doc: ExtendedDocument): DamageRoll[] => {
  return extractDamageRolls('or ', ' * two hands')()(doc)
}

const extractExtraDamage = (doc: ExtendedDocument): DamageRoll[] => {
  return extractDamageRolls(`(and|plus|extra) `, '')()(doc)
}

const extractOngoingDamage = (doc: ExtendedDocument): DamageRoll[] => {
  const sentences = doc.sentences()

  const matchers = [
    { prefix: '', suffix: ' at the start of each * (turn|turns)' },
    { prefix: 'at the start of each * (turn|turns) * takes ', suffix: '' },
    { prefix: '', suffix: ' for? every #Value? #Duration' },
    { prefix: 'every #Value? #Duration * ', suffix: '' },
  ]
  let damage: DamageRoll[] = []
  sentences.forEach((s) => {
    for (const m of matchers) {
      const r = extractDamageRolls(m.prefix, m.suffix)(true)(s)
      if (r.length > 0) {
        damage = [...damage, ...r]
        break
      }
    }
  })
  return damage
}

export { extractMainDamage, extractVersatileDamage, extractExtraDamage, extractOngoingDamage }
