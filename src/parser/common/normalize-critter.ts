import { SKILL_ABILITY_MAP } from '@parser/constants'
import { AbilityScoreModifiers, critterDB } from '@typing'
import { abilityScoreToModifier } from '@utils'

type PartialSkill = Omit<critterDB.Skill, 'modifier'>
type PartialSavingThrow = Omit<critterDB.SavingThrow, 'modifier'>

type PartialStats = Omit<
  critterDB.Stats,
  | 'abilityScoreModifiers'
  | 'extraHealthFromConstitution'
  | 'hitPoints'
  | 'passivePerception'
  | 'skills'
  | 'savingThrows'
> & { skills: PartialSkill[]; savingThrows: PartialSavingThrow[] }

type PartialCritter = Omit<critterDB.Critter, 'stats'> & { stats: PartialStats }

const emptyCritter: critterDB.Critter = {
  _id: '',
  __v: 0,
  name: '',
  sharing: { linkSharingEnabled: false },
  flavor: {
    description: '',
    environment: '',
    faction: '',
    imageUrl: '',
    nameIsProper: false,
    descriptionHtml: {},
  },
  stats: {
    size: '',
    race: '',
    alignment: '',
    armorType: '',
    armorClass: 17,
    numHitDie: 18,
    speed: '',
    abilityScores: {
      strength: 21,
      dexterity: 9,
      constitution: 15,
      intelligence: 18,
      wisdom: 15,
      charisma: 18,
    },
    proficiencyBonus: 0,
    damageVulnerabilities: [],
    damageResistances: [],
    damageImmunities: [],
    conditionImmunities: [],
    senses: [],
    languages: [],
    challengeRating: 0,
    experiencePoints: 0,
    legendaryActionsPerRound: 0,
    legendaryActionsDescription: '',
    savingThrows: [],
    skills: [],
    additionalAbilities: [],
    actions: [],
    reactions: [],
    legendaryActions: [],
    hitDieSize: 0,
    // The following items need to be calculated
    abilityScoreModifiers: {
      strength: 0,
      dexterity: 0,
      constitution: 0,
      intelligence: 0,
      wisdom: 0,
      charisma: 0,
    },
    extraHealthFromConstitution: 0,
    hitPoints: 0,
    passivePerception: 0,
  },
}

const deriveSkillModifier =
  (abilityMods: AbilityScoreModifiers, proficiency: number) =>
  (skill: Partial<critterDB.Skill>): critterDB.Skill =>
    ({
      ...skill,
      modifier: skill.modifier
        ? skill.modifier
        : skill.value
        ? skill.value
        : abilityMods[SKILL_ABILITY_MAP[(skill.name ?? '').toLowerCase()] as keyof AbilityScoreModifiers]
          + (skill.proficient ? proficiency : 0),
    } as critterDB.Skill)

const deriveSaveModifier =
  (abilityMods: AbilityScoreModifiers, proficiency: number) =>
  (save: Partial<critterDB.SavingThrow>): critterDB.SavingThrow =>
    ({
      ...save,
      modifier: save.modifier
        ? save.modifier
        : save.value
        ? save.value
        : abilityMods[(save.ability ?? '') as keyof AbilityScoreModifiers] + (save.proficient ? proficiency : 0),
    } as critterDB.SavingThrow)

const normalizeCritter = (critter: PartialCritter): critterDB.Critter => {
  const {
    stats: { abilityScores, numHitDie, hitDieSize, proficiencyBonus, skills: _skills, savingThrows: _savingThrows },
  } = critter

  const abilityScoreModifiers = Object.fromEntries(
    Object.entries(abilityScores).map(([k, v]) => [k, abilityScoreToModifier(v)])
  ) as unknown as AbilityScoreModifiers

  const deriveSkill = deriveSkillModifier(abilityScoreModifiers, proficiencyBonus)
  const deriveSave = deriveSaveModifier(abilityScoreModifiers, proficiencyBonus)
  const skills = _skills.map((s) => deriveSkill(s))
  const savingThrows = _savingThrows.map((s) => deriveSave(s))

  const extraHealthFromConstitution = numHitDie * abilityScoreModifiers.constitution
  const hitPoints = Math.floor(((hitDieSize + 1) / 2) * numHitDie + extraHealthFromConstitution)

  const perception =
    skills.find((s) => s.name.toLowerCase() === 'perception')?.value ?? abilityScoreModifiers.wisdom + proficiencyBonus
  const passivePerception = 10 + perception

  const flavor = { ...emptyCritter.flavor, ...critter.flavor }
  const stats = {
    ...emptyCritter.stats,
    ...critter.stats,
    abilityScoreModifiers,
    extraHealthFromConstitution,
    hitPoints,
    passivePerception,
    skills,
    savingThrows,
  }

  const normalized: critterDB.Critter = {
    ...emptyCritter,
    ...critter,
    flavor,
    stats,
  }

  return normalized
}

export { normalizeCritter }
