import { NamedUnitValue } from '@typing/statblock-5e'
import { ExtendedDocument, nlp } from '@parser/nlp'

const extractNamedUnitValue = (doc: ExtendedDocument): NamedUnitValue[] => {
  const values: NamedUnitValue[] = []

  const terms = doc.termList()
  for (const term of doc.toNumericCardinal!().termList()) {
    if (term.tags.Cardinal) {
      const prev = terms.find((t) => t.id == term.prev)
      const next = terms.find((t) => t.id == term.next)

      const name = prev && !prev.tags.Unit && !prev.tags.NumericValue && prev.clean ? prev.clean : ''
      const unit = next && !next.tags.NumericValue && next.clean ? next.clean : ''

      values.push({
        name,
        value: Number(term.clean),
        unit,
      })
    }
  }

  return values
}

const parseNamedUnitValue = (text: string): NamedUnitValue[] => {
  // make sure there's a space between value and unit
  const prepared = text.replace(/([0-9])([^\s\.,;:0-9])/g, '$1 $2')
  let doc = nlp(prepared)
  return extractNamedUnitValue(doc)
}

export { parseNamedUnitValue }
