import { DamageRoll, Roll } from '@typing'

const rollComparator = (a: Roll, b: Roll) => a.formula === b.formula

const damageRollComparator = (a: DamageRoll, b: DamageRoll) => rollComparator(a, b) && a.damageType === b.damageType

export { rollComparator, damageRollComparator }
