import { nlp } from '@parser/nlp'
import { Charges } from '@typing'
import { stripHtml } from '@utils'

const prepareText = (text: string) => stripHtml(text.replace(/-|\.|\//g, ' ')).toLowerCase()

const matchCharges = `[<charges>#Value] [<reset>#ChargeReset? #ChargeReset]`

const extractCharges = (text: string): Charges => {
  const match = nlp(prepareText(text)).match(matchCharges)

  return {
    count: Number(match.groups('charges').toNumericCardinal!().text() || 0),
    reset: match.groups('reset').text(),
  }
}

export { extractCharges }
