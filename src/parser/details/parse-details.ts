import { ParserOptions } from '@parser/options'
import { critterDB, Details } from '@typing'
import { crFromProficiency } from '@utils'
import { parseRace } from './race'

const parseDetails =
  ({ conformProficiency }: ParserOptions) =>
  (critter: critterDB.Critter): Details => {
    const {
      flavor: { faction, environment },
      stats: { size, alignment, challengeRating, experiencePoints: xp, proficiencyBonus },
    } = critter
    const cr =
      conformProficiency == 'conformCrToProf' ? crFromProficiency(proficiencyBonus, challengeRating) : challengeRating
    return {
      size,
      faction,
      environment,
      race: parseRace(critter),
      alignment,
      cr,
      xp,
    }
  }

export { parseDetails }
