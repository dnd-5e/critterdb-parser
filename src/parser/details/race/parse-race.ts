import { ExtendedDocument, nlp } from '@parser/nlp'
import { critterDB, Race } from '@typing'

const matchSwarm = `swarm * [<size>#Size] [<type>.]`
const matchSubtype = /\((.*)\)/

const extractSwarm = (doc: ExtendedDocument): Partial<Race> => {
  const swarm = doc.match(matchSwarm)
  return swarm.found
    ? {
        type: swarm.groups('type').text() ?? '',
        swarmsize: swarm.groups('size').text() ?? '',
      }
    : {}
}

const parseRace = (critter: critterDB.Critter): Race => {
  const {
    stats: { race },
  } = critter

  const subtype =
    race
      .match(matchSubtype)?.[1]
      ?.split(',')
      .map((s) => s.trim()) ?? []

  const doc = nlp(race)

  return {
    type: race.replace(/\(.*\)/g, '').trim(),
    subtype,
    swarmsize: '',
    ...extractSwarm(doc),
  }
}

export { parseRace }
