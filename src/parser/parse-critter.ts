import { parseAbillties } from '@parser/abilities'
import { parseDefense } from '@parser/defense'
import { parseDetails } from '@parser/details'
import { ParserOptions } from '@parser/options'
import { parseStats } from '@parser/stats'
import { parseTraits } from '@parser/traits'
import { Abilities, Critter, critterDB, Defense, Details, Statblock5e, Stats, Traits } from '@typing'

const parseCritter =
  (options: ParserOptions) =>
  (critter: critterDB.Critter): Critter => {
    const {
      _id,
      __v,
      name,
      bestiaryId,
      publishedBestiaryId,
      sharing,
      flavor: { description, imageUrl },
    } = critter

    const stats: Stats = parseStats(options)(critter)

    const details: Details = parseDetails(options)(critter)

    const traits: Traits = parseTraits(critter)

    const defense: Defense = parseDefense(critter)

    const abilities: Abilities = parseAbillties(options)(critter)

    const statblock: Statblock5e = {
      name,
      description,
      imageUrl,
      details,
      traits,
      stats,
      defense,
      abilities,
    }

    const parsed: Critter = {
      _id,
      __v,
      bestiaryId,
      publishedBestiaryId,
      sharing,
      statblock,
    }

    return parsed
  }

export { parseCritter }
