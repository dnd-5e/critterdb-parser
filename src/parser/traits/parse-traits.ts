import { critterDB, Traits } from '@typing'
import { parseMovement } from './movement'
import { parseSenses } from './senses'

const parseTraits = (critter: critterDB.Critter): Traits => {
  const {
    stats: { languages, senses, speed },
  } = critter
  return {
    languages,
    senses: parseSenses(senses.join(', ')),
    speed: parseMovement(speed),
  }
}

export { parseTraits }
