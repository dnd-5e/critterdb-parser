import { parseNamedUnitValue } from '@parser/common'
import { Sense } from '@typing/statblock-5e'

const parseSenses = (text: string): Sense[] => {
  return parseNamedUnitValue(text)
}

export { parseSenses }
