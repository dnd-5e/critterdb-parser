import { parseNamedUnitValue } from '@parser/common'
import { Speed } from '@typing/statblock-5e'

const parseMovement = (text: string): Speed[] => {
  return parseNamedUnitValue(text)
}

export { parseMovement }
