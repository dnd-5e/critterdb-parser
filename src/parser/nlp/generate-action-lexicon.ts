import { Lexicon } from '@parser/constants'

const generateActionLexicon = (actions: string[]): Lexicon => {
  const lexicon: Lexicon = {}
  for (const a of actions) {
    lexicon[a] = 'Action'
  }

  return lexicon
}

export { generateActionLexicon }
