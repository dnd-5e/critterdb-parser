import { Lexicon } from '@parser/constants'

const parseLexicon = (lexicon: Lexicon): { [x: string]: string }[] => {
  const lexicons: { [x: string]: string }[] = []
  const mainLexicon: { [x: string]: string } = {}
  for (const [term, tags] of Object.entries(lexicon)) {
    if (Array.isArray(tags)) {
      for (const tag of tags) {
        lexicons.push({ [term]: tag })
      }
    } else {
      mainLexicon[term] = tags
    }
  }
  return [mainLexicon, ...lexicons]
}

export { parseLexicon }
