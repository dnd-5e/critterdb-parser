import { Lexicon, LEXICON_5E } from '@parser/constants'
import { normalizeMath, stripHtml } from '@utils'
import nlp from 'compromise'
import numbers from 'compromise-numbers'
import { parseLexicon } from './parse-lexicon'
import { ExtendedDocument, ExtendedNlp, Numbers } from './type-shims'

const getNlp = (lexicon: Lexicon): ExtendedNlp =>
  nlp
    .extend((Doc: nlp.Document & { prototype: any }, world: nlp.World) => {
      for (const l of parseLexicon(lexicon)) {
        world.addWords(l)
      }

      // add utility method fix units (turns '60ft' into '60 ft')
      Doc.prototype.fixUnits = function () {
        return nlp(this.text().replace(/([0-9])([^\/\s\.,;:0-9])/g, '$1 $2'))
      }

      // add utility method to strip html
      Doc.prototype.stripHtml = function () {
        return nlp(stripHtml(this.text()))
      }

      // add utility method to strip hyphens
      Doc.prototype.stripHyphens = function () {
        return nlp(this.text().replace(/-/g, ' '))
      }

      // add utility method to normalize math strings
      Doc.prototype.normalizeMath = function () {
        return nlp(normalizeMath(this.text()))
      }

      // TODO: try and fix compromise typings
      // utility function that also avoids some type issues
      Doc.prototype.toNumericCardinal = function () {
        return this.numbers().toCardinal().toNumber() as ExtendedDocument
      }
    })
    .extend(numbers)

const exNlp: ExtendedNlp = getNlp(LEXICON_5E)

export { exNlp as nlp, Numbers, getNlp }
