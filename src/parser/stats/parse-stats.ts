import { ParserOptions } from '@parser/options'
import { critterDB, Stats } from '@typing'
import { proficiencyFromCr } from '@utils'
import { parseLegendaryResistances } from './legendary-resistances'
import { parseSavingThrow } from './saving-throws'
import { parseSkill } from './skills'

const parseStats =
  (options: ParserOptions) =>
  (critter: critterDB.Critter): Stats => {
    const { conformProficiency, enforceMinimumProficiency } = options
    const {
      stats: {
        proficiencyBonus: prof,
        passivePerception,
        abilityScores,
        abilityScoreModifiers,
        skills,
        savingThrows,
        challengeRating: cr,
        additionalAbilities,
      },
    } = critter

    const proficiencyBonus =
      conformProficiency == 'conformProfToCr' ? proficiencyFromCr(cr) : enforceMinimumProficiency && prof < 2 ? 2 : prof

    const skillParser = parseSkill(options)(proficiencyBonus, abilityScoreModifiers)
    const saveParser = parseSavingThrow(options)(proficiencyBonus, abilityScoreModifiers)

    const legendaryResistances = parseLegendaryResistances(additionalAbilities)
    return {
      proficiencyBonus,
      passivePerception,
      abilityScores,
      abilityScoreModifiers,
      skills: skills.map((s) => skillParser(s)),
      savingThrows: savingThrows.map((s) => saveParser(s)),
      legendaryResistances,
    }
  }

export { parseStats }
