import { ParserOptions } from '@parser/options'
import { AbilityScoreModifiers, critterDB, SavingThrow } from '@typing'
import { hasProficiency } from '@utils'

const parseSavingThrow =
  ({ deriveProficiencyFromModifiers }: ParserOptions) =>
  (proficiency: number, abilities: AbilityScoreModifiers) =>
  (save: critterDB.SavingThrow): SavingThrow => {
    const { ability, modifier: value, proficient: prof } = save

    const abilityMod = abilities[ability as keyof AbilityScoreModifiers] ?? 0

    const proficient = deriveProficiencyFromModifiers ? prof || hasProficiency(value, proficiency, abilityMod) : prof

    return {
      ability,
      value,
      proficient,
    }
  }

export { parseSavingThrow }
