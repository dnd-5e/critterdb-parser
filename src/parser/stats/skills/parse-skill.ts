import { SKILL_ABILITY_MAP } from '@parser/constants'
import { ParserOptions } from '@parser/options'
import { AbilityScoreModifiers, critterDB, Skill } from '@typing'
import { hasExpertise, hasProficiency } from '@utils'

const parseSkill =
  ({ deriveProficiencyFromModifiers }: ParserOptions) =>
  (proficiency: number, abilities: AbilityScoreModifiers) =>
  (skill: critterDB.Skill): Skill => {
    const { name, modifier: value, proficient: prof } = skill

    const ability = SKILL_ABILITY_MAP[name.toLowerCase()] ?? ''
    const abilityMod = abilities[ability as keyof AbilityScoreModifiers] ?? 0

    const expertise = deriveProficiencyFromModifiers ? hasExpertise(value, proficiency, abilityMod) : false
    const proficient = deriveProficiencyFromModifiers
      ? expertise || prof || hasProficiency(value, proficiency, abilityMod)
      : prof

    return {
      name,
      ability,
      value,
      proficient,
      expertise,
    }
  }

export { parseSkill }
