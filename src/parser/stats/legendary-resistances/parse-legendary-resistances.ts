import { extractCharges } from '@parser/common'
import { critterDB } from '@typing'

const parseLegendaryResistances = (abilities: critterDB.Ability[]): number => {
  const feature = abilities.find((a) => a.name.toLowerCase().includes('legendary resistance'))
  return feature ? extractCharges(feature.name).count : 0
}

export { parseLegendaryResistances }
