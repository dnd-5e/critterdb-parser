type Lexicon = { [x: string]: string | string[] }

const LEXICON_5E: { [x: string]: string | string[] } = {
  // General
  dc: 'DifficultyClass',

  // Abilities
  strength: 'Ability',
  str: 'Ability',
  dexterity: 'Ability',
  dex: 'Ability',
  constitution: 'Ability',
  con: 'Ability',
  intelligence: 'Ability',
  int: 'Ability',
  wisdom: 'Ability',
  wis: 'Ability',
  charisma: 'Ability',
  cha: 'Ability',

  // Damage types
  bludgeoning: 'DamageType',
  piercing: 'DamageType',
  slashing: 'DamageType',
  acid: 'DamageType',
  cold: 'DamageType',
  fire: 'DamageType',
  force: 'DamageType',
  lightning: 'DamageType',
  necrotic: 'DamageType',
  poison: 'DamageType',
  psychic: 'DamageType',
  radiant: 'DamageType',
  thunder: 'DamageType',

  // Attack Types
  melee: 'AttackType',
  ranged: 'AttackType',

  // Movement Types
  burrow: 'MovementType',
  climb: 'MovementType',
  fly: 'MovementType',
  swim: 'MovementType',

  // Sense Types
  darkvision: 'Sense',
  blindsight: 'Sense',
  tremorsense: 'Sense',
  truesight: 'Sense',

  // Size Types
  tiny: 'Size',
  small: 'Size',
  medium: 'Size',
  large: 'Size',
  huge: 'Size',
  gargantuan: 'Size',

  line: 'AreaShape',
  cone: 'AreaShape',
  circle: 'AreaShape',
  square: 'AreaShape',
  sphere: 'AreaShape',
  cube: 'AreaShape',
  cylinder: 'AreaShape',
  radius: 'AreaShape',
  wall: 'AreaShape',

  day: ['ChargeReset', 'Duration'],
  dawn: 'ChargeReset',
  'short rest': 'ChargeReset',
  'long rest': 'ChargeReset',

  round: 'Duration',
  rounds: 'Duration',
  turn: 'Duration',
  turns: 'Duration',
  second: 'Duration',
  seconds: 'Duration',
  minute: 'Duration',
  minutes: 'Duration',
  hour: 'Duration',
  hours: 'Duration',
  days: 'Duration',
  week: 'Duration',
  weeks: 'Duration',
  month: 'Duration',
  months: 'Duration',
  year: 'Duration',
  years: 'Duration',

  '+': 'Operator',
  '-': 'Operator',
}

export { LEXICON_5E, Lexicon }
