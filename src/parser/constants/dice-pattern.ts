// only matches the dice, not modifiers, but since the nlp grabs
// the entire string as a single term anyway it doesn't matter.
const dicePattern = '/([1-9][0-9]*?d[0-9]+)/'

export { dicePattern }
