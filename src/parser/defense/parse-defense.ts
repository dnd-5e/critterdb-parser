import { critterDB, Defense } from '@typing'

const parseDefense = (critter: critterDB.Critter): Defense => {
  const {
    stats: {
      hitDieSize,
      numHitDie: hitDieCount,
      extraHealthFromConstitution: hitPointBonus,
      hitPoints,
      armorType,
      armorClass,
      damageResistances,
      damageVulnerabilities,
      damageImmunities,
      conditionImmunities,
    },
  } = critter
  return {
    damageVulnerabilities,
    damageResistances,
    damageImmunities,
    conditionImmunities,
    armorClass,
    armorType: armorType ?? '',
    hitPoints,
    hitDieSize,
    hitDieCount,
    hitPointBonus,
  }
}

export { parseDefense }
