const hasExpertise = (value: number, proficiency: number, ability: number): boolean => {
  return (value - ability) / 2 >= proficiency
}

export { hasExpertise }
