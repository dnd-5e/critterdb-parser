const proficiencyFromCr = (cr: string | number): number => {
  const rating = ['1/8', '1/4', '1/2'].includes(cr as string) ? 0 : Number(cr)
  return Math.max(2, 1 + Math.ceil(rating / 4))
}

export { proficiencyFromCr }
