const arrayDiff = <T>(items: T[], exclude: T[], comparator: (a: T, b: T) => boolean): T[] => {
  const cleaned: T[] = []

  for (const r of items) {
    let exists = false
    for (const [j, e] of exclude.entries()) {
      if (comparator(e, r)) {
        exists = true
        exclude.splice(j, 1)
        break
      }
    }
    if (!exists) {
      cleaned.push(r)
    }
  }
  return cleaned
}

export { arrayDiff }
