const splitSpellList = (text: string): string[] => {
  // first we split by comma, accepting the inevitability that
  // someone will eventually put a comma in a spell name and
  // burn our hopes and dreams to the ground
  const items = text.split(',').map((i) => i.trim())

  // check if the last item starts with 'and' to deal
  // with oxford commas.
  let lastItem = items[items.length - 1]
  lastItem = lastItem.startsWith('and') ? lastItem.substring(3, lastItem.length).trim() : lastItem
  items[items.length - 1] = lastItem

  // Then we pray that people will not end their list with
  // a spell with 'and' in the name, knowing our prayer will
  // not be answered
  if (lastItem.match(/and/g)?.length == 2) {
    const parts = lastItem.split('and').map((i) => i.trim())
    return items.slice(0, -1).concat([parts.slice(0, -1).join(' and '), parts[2]])
  } else if (lastItem.match(/and/g)?.length == 1) {
    return items.slice(0, -1).concat(lastItem.split('and').map((i) => i.trim()))
  } else {
    return items
  }
}

export { splitSpellList }
