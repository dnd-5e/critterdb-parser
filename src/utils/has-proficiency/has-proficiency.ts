const hasProficiency = (value: number, proficiency: number, ability: number): boolean => {
  return value - ability >= proficiency
}

export { hasProficiency }
