const abilityScoreToModifier = (score: number) => Math.floor((score - 10) / 2)
export { abilityScoreToModifier }
