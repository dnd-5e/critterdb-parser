import { proficiencyFromCr } from '@utils/proficiency-from-cr'

const crFromProficiency = (prof: number, cr: string | number): number => {
  const proficiency = prof < 2 ? 2 : prof
  let rating = ['1/8', '1/4', '1/2'].includes(cr as string) ? 0 : Number(cr)
  while (proficiencyFromCr(rating) != proficiency) {
    rating += proficiencyFromCr(rating) < proficiency ? 1 : -1
  }
  return rating
}

export { crFromProficiency }
