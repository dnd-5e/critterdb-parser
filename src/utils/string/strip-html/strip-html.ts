const stripHtml = (text: string) => {
  return text.replace(/(<([^>]+)>)/gi, '')
}

export { stripHtml }
