const removeCharacterAt = (string: string, index: number) => {
  return string.slice(0, index) + string.slice(index + 1)
}

export { removeCharacterAt }
