import { removeCharacterAt } from '@utils/string/remove-character-at'

const normalizeMath = (text: string): string => {
  for (let i = 0; i < text.length; i++) {
    if (/[+|-]/.test(text[i])) {
      if (text[i - 1] === ' ' && /[0-9]/.test(text[i - 2])) {
        text = removeCharacterAt(text, i - 1)
        i--
      }
      if (text[i + 1] === ' ' && /[0-9]/.test(text[i + 2])) {
        text = removeCharacterAt(text, i + 1)
        i--
      }
    }
  }
  return text
}

export { normalizeMath }
