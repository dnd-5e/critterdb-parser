interface Flavor {
  faction: string
  environment: string
  description: string
  nameIsProper: boolean
  imageUrl: string
  descriptionHtml?: any
}

export { Flavor }
