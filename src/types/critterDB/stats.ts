import { AbilityScoreModifiers, AbilityScores, AbilityScoreStrs } from './ability-scores'
import { Ability, Action } from './action'
import { SavingThrow, Skill } from './skill-save'

interface Stats {
  size: string
  race: string
  alignment: string
  armorType: string | null
  armorTypeStr?: string
  armorClass: number
  numHitDie: number
  speed: string
  abilityScores: AbilityScores
  abilityScoreModifiers: AbilityScoreModifiers
  abilityScoreStrs?: AbilityScoreStrs
  proficiencyBonus: number
  damageVulnerabilities: string[]
  damageResistances: string[]
  damageImmunities: string[]
  conditionImmunities: string[]
  senses: string[]
  languages: string[]
  challengeRating: number
  challengeRatingStr?: string
  experiencePoints: number
  legendaryActionsPerRound: number
  legendaryActionsDescription: string
  savingThrows: SavingThrow[]
  skills: Skill[]
  additionalAbilities: Ability[]
  actions: Action[]
  reactions: Action[]
  legendaryActions: Action[]
  hitDieSize: number
  extraHealthFromConstitution: number
  hitPoints: number
  hitPointsStr?: string
  passivePerception: number
}

export { Stats }
