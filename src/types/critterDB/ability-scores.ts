import { Stringified } from '../type-utils'

interface AbilityScores {
  strength: number
  dexterity: number
  constitution: number
  intelligence: number
  wisdom: number
  charisma: number
}

type AbilityScoreModifiers = AbilityScores

type AbilityScoreStrs = Stringified<AbilityScores>

export { AbilityScores, AbilityScoreModifiers, AbilityScoreStrs }
