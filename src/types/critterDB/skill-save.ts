interface SkillBase {
  value?: number
  proficient: boolean
  modifier: number
  modifierStr?: string
  $$hashKey?: string
}

interface SavingThrow extends SkillBase {
  ability: string
}

interface Skill extends SkillBase {
  name: string
}

export { Skill, SavingThrow }
