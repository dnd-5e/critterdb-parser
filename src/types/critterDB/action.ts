interface Action {
  name: string;
  description: string;
  descriptionHtml: any;
  $$hashKey: string;
}

type Ability = Action;

export { Action, Ability };
