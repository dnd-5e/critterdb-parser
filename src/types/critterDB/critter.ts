import { Flavor } from './flavor'
import { SharingData } from './sharing'
import { Stats } from './stats'

interface Critter {
  _id: string
  __v: number
  name: string
  bestiaryId?: string
  publishedBestiaryId?: string
  flavor: Flavor
  stats: Stats
  sharing: SharingData
}

export type { Critter }
