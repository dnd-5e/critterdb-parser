import { SharingData } from './sharing'

interface Bestiary {
  _id: string
  __v: number
  ownerId: string
  name: string
  description: string
  lastActive: string
  sharing: SharingData
}

export { Bestiary }
