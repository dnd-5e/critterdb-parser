type Stringified<Type> = {
  [Property in keyof Type]: string;
};

export type { Stringified };
