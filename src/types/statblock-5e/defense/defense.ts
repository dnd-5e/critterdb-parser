interface Defense {
  damageVulnerabilities: string[]
  damageResistances: string[]
  damageImmunities: string[]
  conditionImmunities: string[]
  armorClass: number
  armorType: string
  hitPoints: number
  hitDieSize: number
  hitDieCount: number
  /** Extra hit points from constitution */
  hitPointBonus: number
}

export { Defense }
