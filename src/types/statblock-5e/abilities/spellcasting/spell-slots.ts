interface SpellSlots {
  [level: number]: number
}

export { SpellSlots }
