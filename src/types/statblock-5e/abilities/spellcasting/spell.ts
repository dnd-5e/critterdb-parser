interface Spell {
  name: string
  atWill: boolean
  level: number
  uses: number
}

export { Spell }
