import { Feature } from '../feature'
import { Spell } from './spell'
import { SpellSlots } from './spell-slots'

interface Spellcasting extends Feature {
  ability: string
  innate: boolean
  level: number
  save: number
  bonus: number
  pact: boolean
  slots: SpellSlots
  spells: Spell[]
}

export { Spellcasting }
