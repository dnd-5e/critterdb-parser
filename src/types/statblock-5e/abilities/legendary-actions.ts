import { Action } from './action'

interface LegendaryActions {
  charges: number
  description: string
  actions: Action[]
}

export { LegendaryActions }
