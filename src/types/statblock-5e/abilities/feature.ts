interface Feature {
  name: string
  description: string
}

export { Feature }
