interface Roll {
  formula: string
}

interface DamageRoll extends Roll {
  damageType: string
  ongoing: boolean
}

export { DamageRoll, Roll }
