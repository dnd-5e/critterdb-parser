import { DamageRoll } from './damage-roll'

interface AbilitySave {
  ability: string
  dc: number
  damage: DamageRoll[]
}

export { AbilitySave }
