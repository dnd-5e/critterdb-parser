import { DamageRoll } from './damage-roll'

interface Attack {
  weapon: boolean
  spell: boolean
  melee: boolean
  ranged: boolean
  bonus: number
  damage: DamageRoll[]
  versatile: DamageRoll[]
}

export { Attack }
