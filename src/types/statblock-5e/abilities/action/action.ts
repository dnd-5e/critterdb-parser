import { UnitValue } from '../../traits'
import { Feature } from '../feature'
import { AbilitySave } from './ability-save'
import { Attack } from './attack'
import { DamageRoll, Roll } from './damage-roll'

type ActivationType = 'action' | 'bonus' | 'reaction' | 'legendary' | 'other' | null

interface LinkedAction {
  count: number
  action: string
}

interface Range {
  normal: number
  max: number
  unit: string
}

interface Target {
  area: UnitValue & { shape: string }
  count: number
  type: string
}

interface Charges {
  count: number
  reset: string
}

interface Action extends Feature {
  activation: ActivationType
  attack: Attack | null
  damageRolls: DamageRoll[]
  savingThrows: AbilitySave[]
  otherRolls: Roll[]
  limitedUses: Charges
  target: Target
  duration: UnitValue
  reach: UnitValue
  range: Range
  recharge: number[]
  cost: number
  linked: LinkedAction[]
}

export { Action, ActivationType, Range, Target, Charges, LinkedAction }
