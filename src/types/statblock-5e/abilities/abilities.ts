import { Action } from './action'
import { LegendaryActions } from './legendary-actions'
import { Spellcasting } from './spellcasting'

interface Abilities {
  spellcastingFeatures: Spellcasting[]
  features: Action[]
  actions: Action[]
  legendaryActions: LegendaryActions
}

export { Abilities }
