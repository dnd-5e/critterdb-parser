interface SavingThrow {
  ability: string
  value: number
  proficient: boolean
}

interface Skill extends SavingThrow {
  name: string
  expertise: boolean
}

export { SavingThrow, Skill }
