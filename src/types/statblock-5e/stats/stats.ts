import { AbilityScoreModifiers, AbilityScores } from './ability-scores'
import { SavingThrow, Skill } from './skills-saves'

interface Stats {
  proficiencyBonus: number
  passivePerception: number
  abilityScores: AbilityScores
  abilityScoreModifiers: AbilityScoreModifiers
  skills: Skill[]
  savingThrows: SavingThrow[]
  legendaryResistances: number
}

export { Stats }
