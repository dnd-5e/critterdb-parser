import { Sense, Speed } from './senses-speed'

interface Traits {
  speed: Speed[]
  senses: Sense[]
  languages: string[]
}

export { Traits }
