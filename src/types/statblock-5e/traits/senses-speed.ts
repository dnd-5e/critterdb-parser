interface UnitValue {
  value: number
  unit: string
}

interface NamedUnitValue extends UnitValue {
  name: string
}

type Speed = NamedUnitValue
type Sense = NamedUnitValue

export { UnitValue, NamedUnitValue, Speed, Sense }
