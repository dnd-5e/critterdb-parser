interface Race {
  type: string
  subtype: string[]
  swarmsize: string
}

export { Race }
