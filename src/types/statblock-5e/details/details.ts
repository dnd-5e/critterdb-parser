import { Race } from './race'

interface Details {
  size: string
  faction: string
  environment: string
  race: Race
  alignment: string
  cr: number
  xp: number
}

export { Details }
