import { Abilities } from './abilities'
import { Defense } from './defense'
import { Details } from './details'
import { Stats } from './stats'
import { Traits } from './traits'

interface Statblock5e {
  name: string
  description: string
  imageUrl: string
  details: Details
  defense: Defense
  traits: Traits
  stats: Stats
  abilities: Abilities
}

export { Statblock5e }
