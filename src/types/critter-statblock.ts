import { Statblock5e } from './statblock-5e'

interface Critter {
  _id: string
  __v: number
  bestiaryId?: string
  publishedBestiaryId?: string
  sharing: {
    linkSharingEnabled: boolean
  }
  statblock: Statblock5e
}

export { Critter }
